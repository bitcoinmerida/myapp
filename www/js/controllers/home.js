'use strict';

app.controller('HomeCtrl', function(Auth, $ionicLoading, $ionicModal, $scope, Like, Match, currentUser, $state) {

  var home = this;
  home.currentIndex = null;
  home.currentCardUid = null;
  home.profiles = [];
  home.currentUser = currentUser;

  $scope.$on('$ionicView.enter', function(e) {
    init();
  });

  function init() {

    var maxAge = JSON.parse(window.localStorage.getItem('maxAge')) || 18;
    var men = JSON.parse(window.localStorage.getItem('men'));
    var women = JSON.parse(window.localStorage.getItem('women'));
    men = men === null? true : men;
    women = women === null? true : women;

    $ionicLoading.show({
      template: '<ion-spinner icon="bubbles"></ion-spinner>'
    });

    Auth.getProfilesByAge(maxAge).$loaded().then(function(data) {
      for (var i = 0; i < data.length; i++) {
        var item = data[i];

        if ((item.gender == 'male' && men) || (item.gender == 'female' && women)) {
          if (item.$id != home.currentUser.$id)
            home.profiles.push(item);
        }
      }

      Like.allLikesByUser(home.currentUser.$id).$loaded().then(function(likesList) {
        home.profiles = _.filter(home.profiles, function(obj) {
          return _.isEmpty(_.where(likesList, {$id: obj.$id}));
        });
        if (home.profiles.length > 0) {
          home.currentIndex = home.profiles.length - 1;
        }
        $ionicLoading.hide();
      });
    });
  }

  home.nope = function(index) {
    home.cardRemoved(index);
  };

  home.like = function(index) {
    home.profile = home.profiles[index];
    Like.addLike(home.currentUser.$id, home.profile.$id);
    Match.checkMatch(home.currentUser.$id, home.profile.$id, $scope);
    home.cardRemoved(index);
    console.log('home.currentUser', home.currentUser);
    console.log('like_uid', home.profile);
    console.log('LIKE');
  };

  home.cardRemoved = function(index) {
    home.profiles.splice(index, 1);

    if (home.profiles.length > 0) {
      home.currentIndex = home.profiles.length - 1;
    }
  };

  $ionicModal.fromTemplateUrl('templates/match-pop.html', {
    scope: $scope
  }).then(function(modal) {
    $scope.matchPopup = modal;
  });

  $ionicModal.fromTemplateUrl('templates/info.html', {
    scope: $scope
  }).then(function(modal) {
    home.modal = modal;
  });

  home.infoWindow = function(index) {
    home.info = home.profiles[index];
    home.modal.show();
  };

  home.closeModal = function() {
    home.modal.hide();
  };

  home.closeMatchPopup = function() {
    $scope.matchPopup.hide();
  };
  $scope.popupCloseStateChange = function (){
    $scope.matchPopup.hide();
    $state.go('app.match');
  }
});
