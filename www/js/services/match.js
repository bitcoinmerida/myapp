'use strict';

app.factory('Match', function($firebaseArray, $ionicPopup, $ionicModal) {

  var ref = firebase.database().ref();

  var Match = {

    //////////////////  function for user match by like ////
    allMatchesByUser: function(uid) {
      return $firebaseArray(ref.child('matches').child(uid));
    },

    /////////////////  function for check match to other user //////
    checkMatch: function(uid1, uid2, scope) {
      var check = ref.child('likes').child(uid2).child(uid1);

      check.on('value', function(snap) {
        if (snap.val() != null) {
          ref.child('matches').child(uid1).child(uid2).set(true);
            scope.matchPopup.show();
        }
      })
    },
    ///////// function for remove match from the user  ////////////
    removeMatch: function(uid1, uid2) {
      ref.child('matches').child(uid1).child(uid2).remove();
    }

  };

  return Match;

});
