'use strict';

app.factory('Like', function($firebaseArray) {

  var ref = firebase.database().ref();

  var Like = {
///////////////////////  function for user likes status ///////////
    allLikesByUser: function(uid) {
      return $firebaseArray(ref.child('likes').child(uid));
    },

    ///////////////////// function for add like of user /////////////
    addLike: function(uid1, uid2) {
      return ref.child('likes').child(uid1).child(uid2).set(true);
    },
    ///////////// function for remove likes from the user /////////
    removeLike: function(uid1, uid2) {
      return ref.child('likes').child(uid1).child(uid2).remove();
    }
  };

  return Like;

});
